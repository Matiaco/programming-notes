import urllib
import xml.etree.ElementTree as ET

address = 'http://python-data.dr-chuck.net/comments_249563.xml'

uh = urllib.urlopen(address)
data = uh.read()
tree = ET.fromstring(data)
results = tree.findall('.//count')

sum = 0

for item in results:
    sum = sum + int(item.text)

print sum
