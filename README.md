# Notes (online courses)

This is a collection of personal notes and other materials available online, that were useful to me to improve my programming skills.

## Algorithms

Notes for courses from Coursera and Udacity.

## Data-Mining

Notes for the course on Data Mining from Coursera.

## Intro_to_data_analysis

Materials from the course on Introduction to Data Analysis from Udacity.

## Intro_to_data_science

Materials from the course on Introduction to Data Science from Udacity

## Udacity

Materials from the course on Machine Learning from Udacity.